
#include "stdafx.h"

#include <malloc.h>

#include "headers/VpiPc.h"
#include "headers/posnetDLL.h"

unsigned int DebitPurchase(const char *commPort, const char *priceInCents, OUT char **hostRespCode)
{
	WORD res;
	COM_PARAMS *connParams = new COM_PARAMS;
	connParams->com = (LPSTR)commPort;
	connParams->baudRate = 19200;
	connParams->byteSize = 8;
	connParams->parity = 'N';
	connParams->stopBits = 1;

	res = vpiOpenPort(connParams);
	if (res == VPI_OK)
	{
		res = vpiTestConnection();
		if (res == VPI_OK)
		{
			PURCHASE_IN *purchaseInParams = new PURCHASE_IN;
			purchaseInParams->amount = (LPSTR)priceInCents;
			purchaseInParams->receiptNumber = (LPSTR)"000000000000";
			purchaseInParams->instalmentCount = (LPSTR)"1";
			purchaseInParams->issuerCode = (LPSTR)"EL";
			purchaseInParams->planCode = (LPSTR)"1";
			purchaseInParams->tip = (LPSTR)"000";
			purchaseInParams->merchantCode = (LPSTR)"37060134";
			purchaseInParams->merchantName = (LPSTR)"AIVI GROUP INTERNACIONA";
			purchaseInParams->cuit = (LPSTR)"30-71437903-4";
			purchaseInParams->linemode = 1;

			TRX_OUT *purchaseOutParams = new TRX_OUT;
			purchaseOutParams->hostRespCode = (LPSTR)malloc(sizeof(char) * 3);
			purchaseOutParams->hostMessage = (LPSTR)malloc(sizeof(char) * 33);
			purchaseOutParams->authCode = (LPSTR)malloc(sizeof(char) * 7);
			purchaseOutParams->ticketNumber = (LPSTR)malloc(sizeof(char) * 8);
			purchaseOutParams->batchNumber = (LPSTR)malloc(sizeof(char) * 4);
			purchaseOutParams->customerName = (LPSTR)malloc(sizeof(char) * 27);
			purchaseOutParams->panFirst6 = (LPSTR)malloc(sizeof(char) * 7);
			purchaseOutParams->panLast4 = (LPSTR)malloc(sizeof(char) * 5);
			purchaseOutParams->date = (LPSTR)malloc(sizeof(char) * 11);
			purchaseOutParams->time = (LPSTR)malloc(sizeof(char) * 9);
			purchaseOutParams->terminalID = (LPSTR)malloc(sizeof(char) * 9);

			res = vpiPurchase(purchaseInParams, purchaseOutParams, 60);
			if (res == VPI_OK)
			{
				*hostRespCode = purchaseOutParams->hostRespCode;
			}
		}

		vpiClosePort();
	}
	
	return res;
}

unsigned int CreditPurchase(const char *commPort, const char *priceInCents, OUT char **hostRespCode)
{
	WORD res;
	COM_PARAMS *connParams = new COM_PARAMS;
	connParams->com = (LPSTR)commPort;
	connParams->baudRate = 19200;
	connParams->byteSize = 8;
	connParams->parity = 'N';
	connParams->stopBits = 1;

	res = vpiOpenPort(connParams);
	if (res == VPI_OK)
	{
		res = vpiTestConnection();
		if (res == VPI_OK)
		{
			PURCHASE_IN *purchaseInParams = new PURCHASE_IN;
			purchaseInParams->amount = (LPSTR)priceInCents;
			purchaseInParams->receiptNumber = (LPSTR)"000000000000";
			purchaseInParams->instalmentCount = (LPSTR)"1";
			purchaseInParams->issuerCode = (LPSTR)"VI";
			purchaseInParams->planCode = (LPSTR)"1";
			purchaseInParams->tip = (LPSTR)"000";
			purchaseInParams->merchantCode = (LPSTR)"37060134";
			purchaseInParams->merchantName = (LPSTR)"AIVI GROUP INTERNACIONA";
			purchaseInParams->cuit = (LPSTR)"30-71437903-4";
			purchaseInParams->linemode = 1;

			TRX_OUT *purchaseOutParams = new TRX_OUT;
			purchaseOutParams->hostRespCode = (LPSTR)malloc(sizeof(char) * 3);
			purchaseOutParams->hostMessage = (LPSTR)malloc(sizeof(char) * 33);
			purchaseOutParams->authCode = (LPSTR)malloc(sizeof(char) * 7);
			purchaseOutParams->ticketNumber = (LPSTR)malloc(sizeof(char) * 8);
			purchaseOutParams->batchNumber = (LPSTR)malloc(sizeof(char) * 4);
			purchaseOutParams->customerName = (LPSTR)malloc(sizeof(char) * 27);
			purchaseOutParams->panFirst6 = (LPSTR)malloc(sizeof(char) * 7);
			purchaseOutParams->panLast4 = (LPSTR)malloc(sizeof(char) * 5);
			purchaseOutParams->date = (LPSTR)malloc(sizeof(char) * 11);
			purchaseOutParams->time = (LPSTR)malloc(sizeof(char) * 9);
			purchaseOutParams->terminalID = (LPSTR)malloc(sizeof(char) * 9);

			res = vpiPurchase(purchaseInParams, purchaseOutParams, 60);
			if (res == VPI_OK)
			{
				*hostRespCode = purchaseOutParams->hostRespCode;
			}
		}

		vpiClosePort();
	}

	return res;
}