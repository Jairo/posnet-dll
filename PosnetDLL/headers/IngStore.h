#ifndef __INGSTORE_H__
#define __INGSTORE_H__

#include <Windows.h>

UINT __stdcall OpenPort(LPCSTR port,UINT baudRate,UINT byteSize,BYTE parity,UINT stopBits);

UINT __stdcall ClosePort();

/***
* @return 0 OK
* @return 1 OK faltan registros
* @return 2 Timeout
* @return 3	Leyo menos
* @return 4 NAK
* @return 5	Respuesta es inv�lida por largo o nombre del comando
*/
UINT __stdcall SendCmd(LPSTR cmd,LPSTR input,WORD inputLen,LPSTR respcode,LPSTR output,
				       WORD maxOutputLen,WORD* outputLen,LONG timeout);

#endif
