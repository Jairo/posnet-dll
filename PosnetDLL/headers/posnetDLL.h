#pragma once

#ifdef POSNETDLL_EXPORTS
#define POSNETDLL __declspec(dllexport)
#else
#define POSNETDLL __declspec(dllimport)
#endif

#define OUT // Dummy macro - Used for function signaling

extern "C" POSNETDLL unsigned int DebitPurchase(const char *commPort, const char *priceInCents, OUT char **hostRespCode);
extern "C" POSNETDLL unsigned int CreditPurchase(const char *commPort, const char *priceInCents, OUT char **hostRespCode);