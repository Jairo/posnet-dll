# posnet-dll

Documentacion

- Visa - Introduccion POS Integrado v1.5
https://drive.google.com/open?id=1cXQJYTn_EPjyJ9GqYl8XX367ie3MpK5J
- Especificacion de interface DLL v1_20
https://drive.google.com/open?id=1DRIqMKCIh1FKJGGnCGx2cR_sn1Qx27SM
- Especificacion protocolo de comunicacion v1_08
https://drive.google.com/open?id=1XlKgBtfKfxz5n6QrFLxhAfPpdcGU7hF2
- Walkthrough: Create and use your own Dynamic Link Library (C++)
https://docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-dynamic-link-library-cpp