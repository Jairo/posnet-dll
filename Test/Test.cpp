// Test.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include "posnetDLL.h"
#include "VpiPc.h"


int main()
{
	std::cout << "Making a $1.00 debit purchase... ";

	char *hostRespCode;
	unsigned int res = DebitPurchase("COM9", "100", &hostRespCode);
	if (res == VPI_OK)
	{
		if (!strcmp(hostRespCode, "00"))
		{
			std::cout << "OK";
		}
		else
		{
			std::cout << "ERROR. Code " << hostRespCode;
		}
		std::cout << std::endl;
	}
	else
	{
		switch (res)
		{
			case VPI_GENERAL_FAIL:
				std::cout << "General Error";
				break;
			case VPI_FAIL:
				std::cout << "Failed";
				break;
			case VPI_TIMEOUT_EXP:
				std::cout << "Timeout Error";
				break;
			case VPI_INVALID_PLAN:
				std::cout << "Invalid Plan";
				break;
			case VPI_TRX_CANCELED:
				std::cout << "Transaction Cancelled";
				break;
			case VPI_DIF_CARD:
				std::cout << "Wrong Card";
				break;
			case VPI_INVALID_CARD:
				std::cout << "Invalid Card";
				break;
			case VPI_EXPIRED_CARD:
				std::cout << "Expired Card";
				break;
			case VPI_ERR_COM:
				std::cout << "Communication Error";
				break;
			case VPI_ERR_PRINT:
				std::cout << "Print Error";
				break;
			case VPI_INVALID_IN_CMD:	// Shouldn't happen
				std::cout << "Invalid Command";
				break;
			case VPI_INVALID_IN_PARAM:	// Shouldn't happen
				std::cout << "Invalid Parameters";
				break;
			case VPI_INVALID_OUT_CMD:	// Shouldn't happen
				std::cout << "Invalid Response";
				break;
			default:					// Shouldn't happen
				std::cout << "Unknown Error";
				break;
		}
		std::cout << std::endl;
	}
	system("PAUSE");
    return 0;
}

